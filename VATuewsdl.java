import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;










public class VATuewsdl
{
  private CloseableHttpClient httpClient;
  private HttpPost httpPost;
  
  public VATuewsdl()
  {
    httpClient = HttpClients.createDefault();
    httpPost = new HttpPost("http://ec.europa.eu/taxation_customs/vies/services/checkVatService");
  }
  









  public String sprawdzNIP(String aKodKraju, String aNip)
    throws UnsupportedEncodingException, ClientProtocolException, IOException, ParserConfigurationException, SAXException
  {
    String wynik = "";
    StringEntity stringEntity = new StringEntity("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ec.europa.eu:taxud:vies:services:checkVat:types\"><soapenv:Header/><soapenv:Body><urn:checkVat><urn:countryCode>" + 
    


      aKodKraju + "</urn:countryCode>" + 
      "<urn:vatNumber>" + aNip + "</urn:vatNumber>" + 
      "</urn:checkVat>" + 
      "</soapenv:Body>" + 
      "</soapenv:Envelope>");
    stringEntity.setContentType("text/xml;charset=UTF-8");
    httpPost.setEntity(stringEntity);
    
    HttpResponse response = httpClient.execute(httpPost);
    HttpEntity resEntity = response.getEntity();
    

    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(resEntity.getContent());
    
    NodeList nList = doc.getElementsByTagName("checkVatResponse");
    for (int temp = 0; temp < nList.getLength(); temp++) {
      Node nNode = nList.item(temp);
      
      if (nNode.getNodeType() == 1) {
        Element eElement = (Element)nNode;
        wynik = eElement.getElementsByTagName("valid").item(0).getTextContent();
        
        if (wynik.equals("true")) {
          wynik = "C";
        } else {
          wynik = "N";
        }
      }
    }
    return wynik;
  }
}
